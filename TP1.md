# I. Prise en main

## 1. Lancer des conteneurs

🌞 lister les conteneurs actifs, et mettre en évidence le conteneur lancé sur le sleep

trouver son nom et ID

```
[root@localhost tp]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                              NAMES
fbd61e415a20        nginx               "sleep 9999"             6 minutes ago       Up 6 minutes        0.0.0.0:32770->80/tcp              confident_robinson
```

🌞 Mise en évidence d'une partie de l'isolation mise en place par le conteneur. Montrer que le conteneur utilise :

une arborescence de processus différente
des cartes réseau différentes
des utilisateurs système différents
des points de montage (les partitions montées) différents

```
[root@localhost tp]# docker exec -it fbd61e415a20 sh
# ls
bin  boot  dev  etc  home  lib  lib64  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
# cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
nginx:x:101:101:nginx user,,,:/nonexistent:/bin/false
 # ifconfig
eth0      Link encap:Ethernet  HWaddr 02:42:AC:11:00:03
          inet addr:172.17.0.3  Bcast:172.17.255.255  Mask:255.255.0.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:7 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:746 (746.0 B)  TX bytes:0 (0.0 B)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
```

🌞 détruire le conteneur avec docker rm

```
[root@localhost ~]# docker stop 2a316b4a1a2b
2a316b4a1a2b
[root@localhost ~]# docker rm 2a316b4a1a2b
2a316b4a1a2b
[root@localhost ~]#
```

🌞 Lancer un conteneur NGINX

utiliser l'image nginx

lancer le conteneur en démon
utiliser -p pour partager un port de l'hôte vers le port 80 du conteneur
visiter le service web en utilisant l'IP de l'hôte (= en utilisant votre partage de port)

```
[root@localhost ~]# docker run -p 80:80 -d nginx
2b10c692f1890de6f91ba0ab6ac021f6bb1cae9e13df332d4d2e5eba0f9013b7
[root@localhost ~]# curl 0.0.0.0:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

## 2. Gestion d'images

🌞 récupérer une image de Apache en version 2.2
la lancer en partageant un port qui permet d'accéder à la page d'accueil d'Apache

```
[root@localhost dockerfile]# docker pull httpd:2.2
2.2: Pulling from library/httpd
Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
Status: Image is up to date for httpd:2.2
docker.io/library/httpd:2.2
```

🌞 créer une image qui lance un serveur web python. L'image doit :

se baser sur alpine (clause FROM)
contenir python3 (clause RUN)
utiliser la clause EXPOSE afin d'être plus explicite

NB : la clause EXPOSE ne partage aucun port, c'est simplement utilisé pour préciser de façon explicite que ce conteneur écoutera sur un port donné
cela permet à quelqu'un qui analyse votre image de savoir rapidement quel port sera utilisé par un conteneur donné

contenir une clause WORKDIR afin de spécifier un répertoire de travail (les commandes suivantes seront lancées depuis ce répertoire)
utiliser COPY pour récupérer un fichier à partager à l'aide du serveur HTTP
lancer la commande python3 -m http.server 8888 (clause CMD)

cela lance un serveur web qui écoute sur le port TCP 8888 de toutes les interfaces du conteneur

```
FROM alpine:latest:wq
RUN apk add python3 && apk add curl
EXPOSE 8080
COPY . /app
WORKDIR /app
CMD python3 -m http.server 8888
```

🌞 lancer le conteneur et accéder au serveur web du conteneur depuis votre PC

avec un docker run et les bonnes options
il faudra faire un partage de port (-p) pour pouvoir partager le port du conteneur vers un port de l'hôte
par exemple docker run -p 7777:8080 <IMAGE> permet de partager le port 7777 de l'hôte vers le port 8080 du conteneur (TCP par défaut)

```
[root@localhost dockerfile]# docker build -t dockerfile .
Sending build context to Docker daemon  4.608kB
Step 1/6 : FROM alpine:latest
 ---> 965ea09ff2eb
Step 2/6 : RUN apk add python3 && apk add curl
 ---> Using cache
 ---> b7eecfe75797
Step 3/6 : EXPOSE 8080
 ---> Using cache
 ---> 48efde05d7cf
Step 4/6 : COPY . /app
 ---> ae49f30ed5d7
Step 5/6 : WORKDIR /app
 ---> Running in 96c6808d9302
Removing intermediate container 96c6808d9302
 ---> 63c5297e4400
Step 6/6 : CMD python3 -m http.server 8888
 ---> Running in b0d12b619065
Removing intermediate container b0d12b619065
 ---> 467cebf62410
Successfully built 467cebf62410
Successfully tagged dockerfile:latest
[root@localhost dockerfile]# docker run -p 8888:8888 -d dockerfile
619c907b2ea05974d5adc8f2c8b356c45d4bb013d9315c0e8551e2db2c879733
[root@localhost dockerfile]# curl 0.0.0.0:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="dockerfile">dockerfile</a></li>
<li><a href="tp/">tp/</a></li>
</ul>
<hr>
</body>
</html>
[root@localhost dockerfile]#
```

🌞 utiliser l'option -v de docker run

c'est un volume Docker

on utilise les volumes pour partager des fichiers de l'hôte dans le conteneur
vous devrez monter le répertoire de votre choix, dans le WORKDIR du conteneur
la syntaxe est la suivante :

```

```

## 3. Manipulation du démon docker

🌞 Modifier la configuration du démon Docker :

modifier le socket utilisé pour la communication avec le démon Docker

trouvez le path du socket UNIX utilisé par défaut (c'est un fichier docker.sock)
utiliser un socket TCP (port TCP) à la place

autrement dit, il faut que votre démon Docker écoute sur un IP:PORT plutôt que sur le path d'un socket UNIX local


prouver que ça fonctionne en manipulant le démon Docker à travers le réseau (depuis une autre machine)


modifier l'emplacement des données Docker

trouver l'emplacement par défaut (c'est le "data-root")
le déplacer dans un répertoire /data/docker que vous créerez à cet effet


modifier le OOM score du démon Docker

renseignez-vous sur l'internet si vous ne savez pas à quoi ça correspond :)
expliquer succintement la valeur choisie

```

```

# II. docker-compose

🌞 Ecrire un docker-compose-v1.yml qui permet de :

lancer votre image de serveur web Python créée en 2.

partage le port TCP du conteneur sur l'hôte
faire en sorte que le conteneur soit build automatiquement si ce n'est pas fait

```
version: "3.7"

services:
  pyweb:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - "8888:8888"
    networks:
      - overlay

networks:
  overlay:
```

🌞 Ajouter un deuxième conteneur docker-compose-v2.yml

ajouter un conteneur NGINX dans le docker-compose-v2.yml

celui-ci doit agir comme reverse proxy vers votre serveur Python

il va falloir produire une configuration NGINX

la configuration doit être monté avec un volume au lancement du conteneur
si vous êtes pas à l'aise avec NGINX et sa config, cf le petit encart en dessous de cette liste

la connexion au conteneur NGINX doit se faire en HTTPS

le certificat et la clé pour la connexion doivent être générés avant le lancement du conteneur
ils sont montés avec un volume au lancement du conteneur

le port du conteneur NGINX doit être exposé sur l'hôte sur le port 443
le port du serveur web n'est plus exposé sur l'hôte

utiliser les aliases network pour que vos conteneurs communiquent entre eux

```
version: "3.7"

services:
  web:
    build:
      context: ./dkr
      dockerfile: Dockerfile
    ports:
      - "8888:8888"
    networks:
      - overlay

  server:
    image: nginx
    volumes:
      - ./nginx/conf/:/etc/nginx/conf.d/
      - ./nginx/certs:/certs
    ports:
      - "443:443"
    networks:
      - overlay

networks:
  overlay:
```

🌞 Le fichier README.md doit contenir des instructions simples sur le lancement de l'application avec docker-compose, et une explication succinte des différents composants

```

```
