# TP2 Containerization in-depth
## I. Gestion de conteneurs Docker

🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker

```
[root@localhost ~]# ps -ef | grep docker
root       1428      1  0 15:12 ?        00:00:04 /usr/bin/dockerd -H fd://
root      45197    963  0 16:24 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/440bb1bd08365fa01d5bb14c3046cd7f7f7336ea6b57178639fc5f3dfc4a32d7 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root      45828    963  0 16:46 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/246e2178d0c95b6f14fe759b82ea70b20f367dd1ab924d10181679b102e65839 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root      46520  46467  0 20:38 pts/0    00:00:00 grep --color=auto docker
[root@localhost ~]# ps -ef | grep containerd
root        963      1  0 15:11 ?        00:00:28 /usr/bin/containerd
root      45197    963  0 16:24 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/440bb1bd08365fa01d5bb14c3046cd7f7f7336ea6b57178639fc5f3dfc4a32d7 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root      45828    963  0 16:46 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/246e2178d0c95b6f14fe759b82ea70b20f367dd1ab924d10181679b102e65839 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
root      46522  46467  0 20:38 pts/0    00:00:00 grep --color=auto containerd
[root@localhost ~]# ps -ef | grep container-shim
root      46524  46467  0 20:38 pts/0    00:00:00 grep --color=auto container-shim
[root@localhost ~]# ps -ef | grep 46524
root      46526  46467  0 20:39 pts/0    00:00:00 grep --color=auto 46524
```

🌞 Utiliser l'API HTTP mise à disposition par dockerd

utiliser un curl (ou autre) pour discuter à travers le socker UNIX
la documentation de l'API est dispo en ligne

récupérer la liste des conteneurs
récupérer la liste des images disponibles

```

```

# II. Sandboxing
## 1. Namespaces
### A. Exploration manuelle
🌞 Trouver les namespaces utilisés par votre shell.

```
[root@localhost ~]# cd /proc/1/ns
[root@localhost ns]# ls -la
total 0
dr-x--x--x. 2 root root 0 Feb  2 20:57 .
dr-xr-xr-x. 9 root root 0 Jan 27 12:47 ..
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 cgroup -> 'cgroup:[4026531835]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 ipc -> 'ipc:[4026531839]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 mnt -> 'mnt:[4026531840]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 net -> 'net:[4026531992]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 pid -> 'pid:[4026531836]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 pid_for_children -> 'pid:[4026531836]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 user -> 'user:[4026531837]'
lrwxrwxrwx. 1 root root 0 Feb  2 20:57 uts -> 'uts:[4026531838]'
```

### B. unshare

On peut créer des namespaces avec l'appel système unshare(). Il existe une commande unshare qui nous permet de l'utiliser directement.
🌞 Créer un pseudo-conteneur à la main en utilisant unshare

lancer une commande unshare


unshare doit exécuter le processus bash

ce processus doit utiliser des namespaces différents de votre hôte :

réseau
mount
PID
user

prouver depuis votre bash isolé que ces namespaces sont bien mis en place

```
[root@localhost ns]# unshare -n -u -m -p -f bash
[root@localhost ns]# lsns
        NS TYPE   NPROCS   PID USER   COMMAND
4026531835 cgroup    192     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026531836 pid       188     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026531837 user      192     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026531838 uts       187     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026531839 ipc       190     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026531840 mnt       182     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026531860 mnt         1    15 root   kdevtmpfs
4026531992 net       187     1 root   /usr/lib/systemd/systemd --system --deserialize 24
4026532497 mnt         1 19920 root   /usr/lib/systemd/systemd-udevd
4026532593 mnt         1   854 root   /sbin/auditd
4026532594 mnt         1 19702 chrony /usr/sbin/chronyd
4026532628 mnt         1   937 root   /usr/sbin/NetworkManager --no-daemon
4026532637 mnt         1 45214 root   sleep 999999
4026532638 uts         1 45214 root   sleep 999999
4026532639 ipc         1 45214 root   sleep 999999
4026532640 pid         1 45214 root   sleep 999999
4026532642 net         1 45214 root   sleep 999999
4026532743 mnt         1 45844 root   sleep 99999
4026532744 uts         1 45844 root   sleep 99999
4026532745 ipc         1 45844 root   sleep 99999
4026532746 pid         1 45844 root   sleep 99999
4026532748 net         1 45844 root   sleep 99999
4026532807 mnt         3 46578 root   unshare -n -u -m -p -f bash
4026532808 uts         3 46578 root   unshare -n -u -m -p -f bash
4026532809 pid         2 46579 root   bash
4026532811 net         3 46578 root   unshare -n -u -m -p -f bash


```

### C. Avec docker
🌞 Trouver dans quels namespaces ce conteneur s'exécute.

```
[root@localhost 45214]# lsns | grep 45214
4026532637 mnt         1 45214 root   sleep 999999
4026532638 uts         1 45214 root   sleep 999999
4026532639 ipc         1 45214 root   sleep 999999
4026532640 pid         1 45214 root   sleep 999999
4026532642 net         1 45214 root   sleep 999999
```

### D. nsenter
🌞 Utiliser nsenter pour rentrer dans les namespaces de votre conteneur en y exécutant un shell

prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage

```

```

### E. Et alors, les namespaces User ?
🌞 Mettez en place la configuration nécessaire pour que Docker utilise les namespaces de type User.

```

```

### F. Isolation réseau ?
🌞 lancer un conteneur simple

je vous conseille une image debian histoire d'avoir des commandes comme ip a qui n'existent pas dans une image allégée comme alpine)
ajouter une option pour partager un port (n'importe lequel), pour voir plus d'informations

docker run -d -p 8888:7777 debian sleep 99999

```
[root@localhost 45214]# docker run -d -p 8888:7777 debian sleep 99999
```

🌞 vérifier le réseau du conteneur

vérifier que le conteneur a bien une carte réseau et repérer son IP

c'est une des interfaces de la veth pair

possible avec un shell dans le conteneur OU avec un docker inspect depuis l'hôte

```

```

🌞 vérifier le réseau sur l'hôte

vérifier qu'il existe une première carte réseau qui porte une IP dans le même réseau que le conteneur
vérifier qu'il existe une deuxième carte réseau, qui est la deuxième interface de la veth pair

son nom ressemble à vethXXXXX@ifXX
identifier les règles iptables liées à la création de votre conteneur

```
[root@localhost 45214]# docker exec -ti 440bb1bd0836 bash
root@5ae471bbd4b6:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
9: eth0@if10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:22:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:2/64 scope link
       valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:0f:e6:1a:11 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:fff:fee6:1a11/64 scope link
       valid_lft forever preferred_lft forever
6: veth09e869f@if04: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 9a:3f:5c:93:6a:d6 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::984f:5cff:fec3:6ad6/64 scope link
       valid_lft forever preferred_lft forever
```

## 2. Cgroups
🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute

```
[root@localhost docker]# ls
246e2178d0c95b6f14fe759b82ea70b20f367dd1ab924d10181679b102e65839
```

### B. Utilisation par Docker
🌞 Lancer un conteneur Docker et trouver

la mémoire RAM max qui lui est autorisée
le nombre de processus qu'il peut contenir
explorer un peu de vous-même ce qu'il est possible de faire avec des cgroups

```
[root@localhost 440bb1bd08365fa01d5bb14c3046cd7f7f7336ea6b57178639fc5f3dfc4a32d7]# cat memory.limit_in_bytes
9223372036854771712
```

🌞 Altérer les valeurs cgroups allouées par défaut avec des options de la commandes docker run (au moins 3)

préciser les options utilisées
prouver en regardant dans /sys qu'elles sont utilisées

```
[root@localhost docker]# docker run -d -m 9999999 debian sleep 99999
```

## 3. Capabilities
### A. Découverte manuelle
🌞 déterminer les capabilities actuellement utilisées par votre shell

```
[root@localhost docker]# cat /proc/1/status | grep Cap
CapInh: 0000000000000000
CapPrm: 0000003fffffffff
CapEff: 0000003fffffffff
CapBnd: 0000003fffffffff
CapAmb: 0000000000000000
```

🌞 Déterminer les capabilities du processus lancé par un conteneur Docker

utiliser quelque chose de simple pour le conteneur comme un docker run -d alpine sleep 99999

en utilisant /proc

```
[root@localhost docker]# capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```

🌞 Jouer avec ping

trouver le chemin absolu de ping

récupérer la liste de ses capabilities
enlever toutes les capabilities

en utilisant une liste vide
setcap '' <PATH>


vérifier que ping ne fonctionne plus
vérifier avec strace que c'est bien l'accès à l'ICMP qui a été enlevé

```
execve("/usr/sbin/ping", ["ping"], 0x7ffca4c4fc40 /* 26 vars */) = 0
brk(NULL)                               = 0x5646771e3000
arch_prctl(0x3001 /* ARCH_??? */, 0x7ffeacca2070) = -1 EINVAL (Invalid argument)
access("/etc/ld.so.preload", R_OK)      = -1 ENOENT (No such file or directory)
openat(AT_FDCWD, "/etc/ld.so.cache", O_RDONLY|O_CLOEXEC) = 3
fstat(3, {st_mode=S_IFREG|0644, st_size=22140, ...}) = 0
mmap(NULL, 22140, PROT_READ, MAP_PRIVATE, 3, 0) = 0x7fec4ede1000
close(3)                                = 0
```

### B. Utilisation par Docker
🌞 lancer un conteneur NGINX qui a le strict nécessaire de capabilities pour fonctionner

prouver qu'il fonctionne
expliquer toutes les capabilities dont il a besoin

```

```
