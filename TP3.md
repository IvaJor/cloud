# TP3 Service Orchestration & Cloud-native environment

# I. Lab setup

Creation d'un template centos suivant cette configuration:


CentOS7
stockage :

un disque de 12go sur toutes les machines
sur node3, un disque supplementaire de 5go

réseau

une carte accès internet
une carte accès à un LAN
hostname défini (Node1, Node2, Node3)


SELinux désactivé en manuel dans /etc/selinux/config
Oouverture des port dockerswarm 2377 
fichier /etc/hosts rempli avec les hostnames des autres noeuds

# II. Mise en place de Docker Swarm

## 1. Setup

🌞 Utilisez des commandes docker afin de créer votre cluster Swarm


docker swarm init pour créer le swarm

utilisez le --advertise-addr pour préciser l'adresse dans le LAN

```
[root@Node1 selinux]# docker swarm init --advertise-addr 10.10.100.149
Swarm initialized: current node (qr3elypnzoni1rgiaa62kzbbo) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-31iw0e5c8jotkxqx5zug50yfmxeboyg5bkxjlaj7kh1nm72jjb-cevxodaypkd9v58sz33hv24ap 10.10.100.149:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```
docker swarm join pour qu'un noeud rejoigne un swarm existant
vos 3 machines doivent être des managers Swarm

```
[root@Node2 ~]# docker swarm join --token SWMTKN-1-31iw0e5c8jotkxqx5zug50yfmxeboyg5bkxjlaj7kh1nm72jjb-11pi29vdv0ufggonqq9hignsz 10.10.100.149:2377
This node joined a swarm as a manager.

[root@Node3 selinux]# docker swarm join --token SWMTKN-1-31iw0e5c8jotkxqx5zug50yfmxeboyg5bkxjlaj7kh1nm72jjb-11pi29vdv0ufggonqq9hignsz 10.10.100.149:2377
This node joined a swarm as a manager.

```

## 2. Une première application orchestrée

🌞 "swarmiser" l'application Python du TP1

récupérez le docker-compose.yml que vous aviez écrit et le déployer dans le Swarm


l'image doit être présente sur tous les noeuds du cluster

vous devez donc la build sur tous les noeuds du cluster

docker build du Dockerfile sur les 3 machines

utiliser un partage de ports pour accéder à l'application

pour rappel, elle écoute sur le pot 8888/TCP

```
[root@Node1 dockerfile]# docker build -t dockerfile .
Sending build context to Docker daemon  3.072kB
Step 1/6 : FROM alpine:latest
latest: Pulling from library/alpine
aad63a933944: Pull complete                                                                                             Digest: sha256:b276d875eeed9c7d3f1cfa7edb06b22ed22b14219a7d67c52c56612330348239
Status: Downloaded newer image for alpine:latest
 ---> a187dde48cd2
Step 2/6 : RUN apk add python3 && apk add curl
 ---> Running in 519be039d97e
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.11/community/x86_64/APKINDEX.tar.gz
(1/10) Installing libbz2 (1.0.8-r1)
(2/10) Installing expat (2.2.9-r1)
(3/10) Installing libffi (3.2.1-r6)
(4/10) Installing gdbm (1.13-r1)
(5/10) Installing xz-libs (5.2.4-r0)
(6/10) Installing ncurses-terminfo-base (6.1_p20200118-r2)
(7/10) Installing ncurses-libs (6.1_p20200118-r2)
(8/10) Installing readline (8.0.1-r0)
(9/10) Installing sqlite-libs (3.30.1-r1)
(10/10) Installing python3 (3.8.2-r0)
Executing busybox-1.31.1-r9.trigger
OK: 64 MiB in 24 packages
(1/4) Installing ca-certificates (20191127-r1)
(2/4) Installing nghttp2-libs (1.40.0-r0)
(3/4) Installing libcurl (7.67.0-r0)
(4/4) Installing curl (7.67.0-r0)
Executing busybox-1.31.1-r9.trigger
Executing ca-certificates-20191127-r1.trigger
OK: 66 MiB in 28 packages
Removing intermediate container 519be039d97e
 ---> e547a856b579
Step 3/6 : EXPOSE 8080
 ---> Running in 5578e7aa5dc2
Removing intermediate container 5578e7aa5dc2
 ---> 33d9917638a9
Step 4/6 : COPY . /app
 ---> c5598fc78c4c
Step 5/6 : WORKDIR /app
 ---> Running in 15d0033949a4
Removing intermediate container 15d0033949a4
 ---> 8b282acf3712
Step 6/6 : CMD python3 -m http.server 8888
 ---> Running in b72bde8bf2f6
Removing intermediate container b72bde8bf2f6
 ---> 1ed522f0c2ee
Successfully built 1ed522f0c2ee
Successfully tagged dockerfile:latest

[root@Node1 dockerfile]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
dockerfile          latest              e7a2010a11a7        48 seconds ago      61.4MB
alpine              latest              a187dde48cd2        2 days ago          5.6MB

[root@Node1 dockerfile]# docker stack deploy -c docker-compose.yml tp
Ignoring unsupported options: build

Creating network tp_overlay
Creating service tp_server
Creating service tp_web
```

🌞 explorer l'application et les fonctionnalités de Swarm

tester le bon fonctionnement de l'application
utiliser ss sur tous les noeuds pour comprendre sur quel port est exposée l'application
utiliser docker service scale afin d'augmenter le nombre de services Python

```
[root@Node2 ~]# ss -6ln
Netid State      Recv-Q Send-Q          Local Address:Port                    Peer Address:Port
icmp6 UNCONN     0      0                           *:58                                 *:* 
udp   UNCONN     0      0                        [::]:5355                            [::]:*
udp   UNCONN     0      0                           *:7946                               *:*
udp   UNCONN     0      0                        [::]:111                             [::]:*
udp   UNCONN     0      0                        [::]:7946                            [::]:*
tcp   LISTEN     0      128                      [::]:8888                            [::]:*
tcp   LISTEN     0      100                     [::1]:25                              [::]:*
tcp   LISTEN     0      128                      [::]:2377                            [::]:*
tcp   LISTEN     0      128                      [::]:7946                            [::]:*
tcp   LISTEN     0      128                      [::]:111                             [::]:*
tcp   LISTEN     0      128                      [::]:22                              [::]:*

[root@Node1 ~]# ss -6ln
Netid State      Recv-Q Send-Q           Local Address:Port                   Peer Address:Port
icmp6 UNCONN     0      0                            *:58                                *:* 
udp   UNCONN     0      0                         [::]:5355                           [::]:*
udp   UNCONN     0      0                            *:7946                              *:*
udp   UNCONN     0      0                         [::]:111                            [::]:*
udp   UNCONN     0      0                         [::]:7946                           [::]:*
tcp   LISTEN     0      128                       [::]:8888                           [::]:*
tcp   LISTEN     0      100                      [::1]:25                             [::]:*
tcp   LISTEN     0      128                       [::]:2377                           [::]:*
tcp   LISTEN     0      128                       [::]:7946                           [::]:*
tcp   LISTEN     0      128                       [::]:111                            [::]:*
tcp   LISTEN     0      128                       [::]:22                             [::]:*

[root@Node3 ~]# ss -6ln
Netid  State      Recv-Q Send-Q           Local Address:Port                  Peer Address:Port
icmp6  UNCONN     0      0                            *:58                               *:* 
udp    UNCONN     0      0                         [::]:5355                          [::]:*
udp    UNCONN     0      0                            *:7946                             *:*
udp    UNCONN     0      0                         [::]:111                           [::]:*
udp    UNCONN     0      0                         [::]:7946                          [::]:*
tcp    LISTEN     0      128                       [::]:8888                          [::]:*
tcp    LISTEN     0      100                      [::1]:25                            [::]:*
tcp    LISTEN     0      128                       [::]:2377                          [::]:*
tcp    LISTEN     0      128                       [::]:7946                          [::]:*
tcp    LISTEN     0      128                       [::]:111                           [::]:*
tcp    LISTEN     0      128                       [::]:22                            [::]:*

```

docker service scale -h pour plus d'infos
vérifier que c'est ok en visitant l'interface web (l'id du conteneur est print sur la page web)
avec un navigateur ou une commande curl

```
[root@node1 ~]# docker service scale tp_web=3
tp_web scaled to 3
overall progress: 3 out of 3 tasks
1/3: running   [==================================================>]
2/3: running   [==================================================>]
3/3: running   [==================================================>]
verify: Service converged

```

trouver sur quel(s) hôte(s) tournent tous les conteneurs lancés

```
[root@Node1 ~]# docker stack ls
NAME                SERVICES            ORCHESTRATOR
tp                  2                   Swarm
[root@Node1 ~]# docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS
ypdon5wuui81        tp_server           replicated          1/1                 nginx:latest        *:443->443/tcp
hdozd5eepocw        tp_web              replicated          1/1                 tp:latest           *:8888->8888/tcp


[root@Node1 ~]# docker stack ps tp
ID                  NAME                 IMAGE               NODE                DESIRED STATE       CURRENT STATE           ERROR                         PORTS
nzpqjvvps6er        tp_web.1             tp:latest           Node2               Running             Running 6 hours ago
psmvir9ex2dp        tp_server.1          nginx:latest        Node1               Running             Running 6 hours ago
msubdoiinzuv        tp_web.2             tp:latest           Node3               Running             Running 27 seconds ago
klmqfjioerig        tp_web.3             tp:latest           Node1               Running             Running 27 seconds ago

```

# III. Construction de l'écosystème

## 1. Registre

🌞 Tester le registre

build l'image contenant l'app Python sur un noeud, en la nommant correctement pour notre registre
pousser l'image de l'application Python
adapter le docker-compose.yml de l'application Python pour utiliser l'image du registre

```
[root@Node1 ~]# cd /data/registry/
[root@Node1 registry]# docker-compose up -d
WARNING: The Docker Engine you re using is running in swarm mode.

Compose does not use swarm mode to deploy services to multiple nodes in a swarm. All containers will be scheduled on the current node.

To deploy your application across the swarm, use `docker stack deploy`.

Creating network "registry_default" with the default driver
Pulling registry (registry:2)...
2: Pulling from library/registry
486039affc0a: Pull complete
ba51a3b098e6: Pull complete
8bb4c43d6c8e: Pull complete
6f5f453e5f2d: Pull complete
42bc10b72f42: Pull complete
Digest: sha256:7d081088e4bfd632a88e3f3bcd9e007ef44a796fddfe3261407a3f9f04abe1e7
Status: Downloaded newer image for registry:2
Creating registry_registry_1 ... done

[root@Node1 ~]# docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
dockerfile          latest              1ed522f0c2ee        21 hours ago        61.4MB
registry            2                   708bc6af7e5e        4 weeks ago         25.8MB
alpine              latest              a187dde48cd2        3 days ago          5.6MB
nginx               <none>              6678c7c2e56c        3 weeks ago         127MB

[root@Node1 ~]# docker tag dockerfile registry.b3:5000/dockerfile/dockerfile:tp

[root@Node1 ~]$ docker push registry.b3:5000/python_app/python_app:tp
The push refers to repository [registry.b3:5000/dockerfile/dockerfile]
158twv94v6sv: Pushed
9813sf45e3e3: Pushed
259ec23q838g: Pushed
35f8r6z8c3e9: Pushed
63d3d6br871e: Pushed
tp: digest: sha256:b2d89d5d3d8g7n2s9887d6d6s6b4sb98t9db4bg9dfg7bf2fgfb6fb6fg86bg4fg size: 1365
                     
```

## 2. Centraliser l'accès aux services

🌞 Déployer une stack Traefik

créer un réseau dédié à Traefik

il sera utilisé pour Traefik
toutes les applications qui auront besoin du reverse proxy Traefik seront aussi dans ce réseau

```
[root@Node1 ~]# docker network create --driver overlay traefik
m37qhhjui1x1jg3pfsuc35nti

[root@Node1 ~]# docker network ls
NETWORK ID          NAME                 DRIVER              SCOPE
a08bae625f82        bridge               bridge              local
a1a46c0d0645        docker_gwbridge      bridge              local
ee0h6lswrlod        dockerfile_overlay   overlay             swarm
d71c92beadd6        host                 host                local
vvmgsb72kl9y        ingress              overlay             swarm
39e57d062cea        none                 null                local
13sergvrvs56        registry_default     bridge              local
odt1i1ur6sjd        tp_overlay           overlay             swarm
m37qhhjui1x1        traefik              overlay             swarm

```

🌞 Déployer une stack Traefik

utiliser le docker-compose.yml fourni

modifier le docker-compose.yml pour partager le port 8080

c'est à dire, ajoutez un 8080:8080 en plus de ce qu'il y a déjà


noter l'utilisation du réseau traefik en external

cela indique qu'il doit être créé auparavant
ce réseau sera aussi utilisé pour les prochaines applications que l'on mettra derrière le reverse proxy


vérifier le bon fonctionnement en visitant l'interface web de Traefik

grâce au partage de port
http://<IP_VM>:8080/dashboard/

faire des tests de bascule en coupant Docker/éteignant une machine

```
[root@Node1 ~]# docker stack deploy -c /data/traefik/docker-compose.yml traefik
Creating service traefik_reverse-proxy

[root@Node1 ~]# docker stack ls
NAME                SERVICES            ORCHESTRATOR
traefik             1                   Swarm

[root@Node1 ~]# docker stack ps traefik
ID                  NAME                      IMAGE               NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
1s684ergsv8u        traefik_reverse-proxy.1   traefik:v2.1.3      Node3            Running             Running 3 hours ago

[root@Node1 ~]# curl Node1:8080/dashboard
<!DOCTYPE html><html><head><title>Traefik</title><meta charset=utf-8><meta name=description content="Traefik UI"><meta name=format-detection content="telephone=no"><meta name=msapplication-tap-highlight content=no><meta name=viewport content="user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width"><link rel=icon type=image/png href=statics/app-logo-128x128.png><link rel=icon type=image/png sizes=16x16 href=statics/icons/favicon-16x16.png><link rel=icon type=image/png sizes=32x32 href=statics/icons/favicon-32x32.png><link rel=icon type=image/png sizes=96x96 href=statics/icons/favicon-96x96.png><link rel=icon type=image/ico href=statics/icons/favicon.ico><link href=css/019be8e4.d05f1162.css rel=prefetch><link href=css/099399dd.9310dd1b.css rel=prefetch><link href=css/0af0fca4.e3d6530d.css rel=prefetch><link href=css/162d302c.9310dd1b.css rel=prefetch><link href=css/29ead7f5.9310dd1b.css rel=prefetch><link href=css/31ad66a3.9310dd1b.css rel=prefetch><link href=css/524389aa.619bfb84.css rel=prefetch><link href=css/61674343.9310dd1b.css rel=prefetch><link href=css/63c47f2b.294d1efb.css rel=prefetch><link href=css/691c1182.ed0ee510.css rel=prefetch><link href=css/7ba452e3.37efe53c.css rel=prefetch><link href=css/87fca1b4.8c8c2eec.css rel=prefetch><link href=js/019be8e4.d8726e8b.js rel=prefetch><link href=js/099399dd.a047d401.js rel=prefetch><link href=js/0af0fca4.271bd48d.js rel=prefetch><link href=js/162d302c.ce1f9159.js rel=prefetch><link href=js/29ead7f5.cd022784.js rel=prefetch><link href=js/2d21e8fd.f3d2bb6c.js rel=prefetch><link href=js/31ad66a3.12ab3f06.js rel=prefetch><link href=js/524389aa.21dfc9ee.js rel=prefetch><link href=js/61674343.adb358dd.js rel=prefetch><link href=js/63c47f2b.caf9b4a2.js rel=prefetch><link href=js/691c1182.5d4aa4c9.js rel=prefetch><link href=js/7ba452e3.71a69a60.js rel=prefetch><link href=js/87fca1b4.ac9c2dc6.js rel=prefetch><link href=css/app.e4fba3f1.css rel=preload as=style><link href=js/app.841031a8.js rel=preload as=script><link href=js/vendor.49a1849c.js rel=preload as=script><link href=css/app.e4fba3f1.css rel=stylesheet><link rel=manifest href=manifest.json><meta name=theme-color content=#027be3><meta name=apple-mobile-web-app-capable content=yes><meta name=apple-mobile-web-app-status-bar-style content=default><meta name=apple-mobile-web-app-title content=Traefik><link rel=apple-touch-icon href=statics/icons/apple-icon-120x120.png><link rel=apple-touch-icon sizes=180x180 href=statics/icons/apple-icon-180x180.png><link rel=apple-touch-icon sizes=152x152 href=statics/icons/apple-icon-152x152.png><link rel=apple-touch-icon sizes=167x167 href=statics/icons/apple-icon-167x167.png><link rel=mask-icon href=statics/icons/safari-pinned-tab.svg color=#027be3><meta name=msapplication-TileImage content=statics/icons/ms-icon-144x144.png><meta name=msapplication-TileColor content=#000000></head><body><div id=q-app></div><script type=text/javascript src=js/app.841031a8.js></script><script type=text/javascript src=js/vendor.49a1849c.js></script></body></html>


```


## 3. Stockage S3

🌞 Préparer l'environnement

utiliser LVM pour partitionner les disques supplémentaires et les monter dans leurs paths respectifs

```
[root@Node1 ~]# lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   12G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   11G  0 part
  ├─cl-root 253:0    0  9,8G  0 lvm  /
  └─cl-swap 253:1    0  1,2G  0 lvm  [SWAP]
sdb               8:16   0    5G  0 disk
└─VGMinio-Minio 253:0    0  4.9G  0 lvm  /minio
sr0          11:0    1 1024M  0 rom
sr1          11:1    1    7G  0 rom

[root@Node2 ~]# lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   12G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   11G  0 part
  ├─cl-root 253:0    0  9,8G  0 lvm  /
  └─cl-swap 253:1    0  1,2G  0 lvm  [SWAP]
sdb               8:16   0    5G  0 disk
└─VGMinio-Minio 253:0    0  4.9G  0 lvm  /minio
sr0          11:0    1 1024M  0 rom
sr1          11:1    1    7G  0 rom

[root@Node3 ~]# lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0   12G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0   11G  0 part
  ├─cl-root 253:0    0  9,8G  0 lvm  /
  └─cl-swap 253:1    0  1,2G  0 lvm  [SWAP]
sdb               8:16   0    5G  0 disk
└─VGMinio-Minio 253:0    0  4.9G  0 lvm  /minio
sdc                     8:32   0    5G  0 disk
└─VGMinio--2-Minio--2 253:1    0  4.9G  0 lvm  /minio-2
sr0          11:0    1 1024M  0 rom
sr1          11:1    1    7G  0 rom


```

🌞 Déployer Minio


déployer Minio en utilisant le docker-compose.yml de la doc

pour la partie avec les labels de noeuds, nous, on a que trois noeuds

```
[root@Node1 ~]# docker node update --label-add minio1=true Node1
Node1
[root@Node1 ~]# docker node update --label-add minio2=true Node2
Node2
[root@Node1 ~]# docker node update --label-add minio3=true Node3
Node3
[root@Node1 ~]# docker node update --label-add minio4=true Node3
Node3


``` 

